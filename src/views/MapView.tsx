import React, { useState } from 'react';
import { MarkerF, useJsApiLoader } from '@react-google-maps/api';
import ContainerWithSidebar from '../components/CointainerWithSidebar';
import Filters from '../components/Filters';
import GoogleMapsComponent from '../components/GoogleMapsComponent';
import { useSpots } from '../hooks/useSpots';
import FancyProgress from '../components/commons/FancyProgress';
import { Spot } from '../types/Spots';
import ResponsiveDialog from '../components/commons/ResponsiveDialog';
import SpotDetailsComponent from '../components/SpotDetailsComponent';


export default function MapView() {
  const { spots } = useSpots()
  const [activeMarker, setActiveMarker] = useState<Spot>({} as Spot);
  const [open, setOpen] = React.useState(false);
  const { isLoaded } = useJsApiLoader({
    id: 'google-map-script',
    googleMapsApiKey: 'AIzaSyDIiocuBPCDmtSX2M8Ech6f3qJrB4iCxJk',
  })

  const handleActiveMarker = (marker: Spot) => {
    setOpen(true);
    setActiveMarker(marker);
  };

  const handleClose = () => {
    setOpen(false);
    setActiveMarker({} as Spot)
  };

  return (
    <React.Fragment>
      <ContainerWithSidebar
        sidebarContent={<Filters />}
        containerContent={
          isLoaded ?
            <GoogleMapsComponent
              center={{ lat: 19.451054, lng: -99.125519 }}
              mapContainerStyle={{ width: "100vw", height: "100vh" }}
              children={
                <div>
                  {
                    spots.map((marker: any, index: any) => {
                      return (
                        <div key={index}>
                          <MarkerF
                            position={{ lat: marker.latitude, lng: marker.longitude }}
                            onClick={(e) => handleActiveMarker(marker)}
                          />
                        </div>
                      )
                    })
                  }
                </div>}
            />
            : <FancyProgress ContainerGridProps={{ width: "100%", height: "100vh" }} size="5rem" />}
      />
      <ResponsiveDialog
        open={open}
        onClose={handleClose}
        title={activeMarker?.name}
        children={<SpotDetailsComponent spot={activeMarker as Spot} />}
      />
    </React.Fragment>
  )
}