import { Spot } from "../types/Spots"

const URL_BASE = process.env.REACT_APP_API
const token = process.env.REACT_APP_TOKEN

export async function fetchSpots(params: URLSearchParams) {
  let allData: Spot[] = [];
  let morePagesAvailable = true;
  let page = 0

  while (morePagesAvailable) {
    page++
    const response = await fetch(`${URL_BASE}api/spots?${new URLSearchParams(params.toString())}&page=${page}`, {
      method: 'GET',
      credentials: 'include',
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    });
    let { data, links } = await response.json();
    data.spots.forEach((e: Spot) => allData.unshift(e));
    morePagesAvailable = links && links.next
  }

  return allData;
}
