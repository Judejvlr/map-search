import { fireEvent, screen } from "@testing-library/react";
import LanguageButtons from "../../components/LanguageButtons"
import { render } from "../../utils/test-utils";


test(('Language buttons render properly'), () => {
  const buttonText = ['EN', 'ES'];
  render(<LanguageButtons />);
  buttonText.map((value) => {
    expect(screen.getByText(value)).toHaveTextContent(value)
  })
})

test(('Language buttons works properly'), () => {
  const buttonText = ['EN', 'ES'];
  render(<LanguageButtons />);
  buttonText.map((value) => {
    expect(screen.getByText(value)).toHaveTextContent(value)
    fireEvent.click(screen.getByText(value))
  })
})