import { fireEvent, screen } from "@testing-library/react";
import SpotDetailsComponent from "../../components/SpotDetailsComponent"
import { render } from "../../utils/test-utils";
import { Spot } from "../../types/Spots";
import { t } from "i18next";


const spot: Spot = {
  id: 67,
  alias: null,
  city_id: 184,
  description: null,
  endorsement: null,
  ext_number: "232",
  floor_level: null,
  front: 0,
  grace_time: null,
  guarantee_deposit: null,
  has_validated_prespot: 0,
  height: 0,
  in_corner: null,
  int_number: null,
  latitude: 19.435646,
  level: 3,
  listing_id: null,
  longitude: -99.200398,
  name: "Polanco 232 Pie de calle (test queue)",
  payload: null,
  pdf_description: null,
  people_flow: null,
  people_flow_term: 3,
  phone: null,
  photos: null,
  reference: "Esq. Horacio y Petrarca",
  relative_url: "pie-de-calle-miguel-hidalgo-polanco-iv-seccion",
  rent_in_advance: null,
  seo: {
    title: "Pie de calle Polanco 232 Pie de calle (test queue) - Spot2",
    description: "Renta Pie de calle para iniciar/expandir tu negocio en Polanco 232 Pie de calle (test queue) Hay 2 personas interesadas en este local. Agenda una visita con nuestros asesores. Contactanos a nuestro Whatsapp.",
    keywords: [
      "Pie de calle en renta",
      "Pie de calle en Miguel Hidalgo Polanco IV Seccion",
      "Pie de calle en Ciudad de Mexico"
    ],
    image: "images/spot-square.jpg"
  },
  square_space: "300.00",
  street: "Av Horacio",
  term: 3,
  total_favorites: 2,
  uuid: "c9d8d9cf-1e2e-4293-99ae-734ca55d684f",
  visual_blocks: null,
  zip_code: {
    code: "11550",
    settlement: "Polanco IV Sección",
    settlement_type: "Colonia",
    municipality: "Miguel Hidalgo",
    city: "Ciudad de México",
    zone: "Urbano"
  },
  created_at: "2021-05-07T21:38:51.000000Z",
  updated_at: "2022-06-27T16:13:17.000000Z"
}

test(('SpotDetailsComponent render properly'), () => {
  render(<SpotDetailsComponent spot={spot} />);
})

test(('SpotDetailsComponent works properly, the info is correct'), () => {
  render(<SpotDetailsComponent spot={spot} />);
  expect(screen.getByText(spot.name)).toHaveTextContent('Polanco 232 Pie de calle (test queue)')
  expect(screen.getByText(spot.zip_code.city + ' - ' + spot.zip_code.municipality)).toHaveTextContent('Ciudad de México - Miguel Hidalgo')
})
