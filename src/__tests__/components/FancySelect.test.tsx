
import { fireEvent, screen, within } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import FancySelect from "../../components/commons/FancySelect"
import { render } from "../../utils/test-utils";
import { SPOTS_TERM } from "../../consts/spots";


test('should display the correct number of options', () => {
  const { getByRole } = render(<FancySelect testId="terms" label={'Terms'} selectItems={SPOTS_TERM} />)
  fireEvent.mouseDown(getByRole('button'));
  const listbox = within(getByRole('listbox'));
  fireEvent.click(listbox.getByRole('option', { name: 'Corto plazo' }));
  expect(getByRole('button')).toHaveTextContent('Corto plazo');
})

