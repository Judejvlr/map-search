import { createContext } from 'react'
import { Spot } from '../types/Spots'

export const SpotsContext = createContext({ spots: [] as Spot[], setSpots: (spots: Spot[]) => { } })

