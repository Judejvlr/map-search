import React from 'react'
import { GoogleMap, GoogleMapProps } from '@react-google-maps/api';


function GoogleMapsComponent(props: GoogleMapProps) {

  return (
    <GoogleMap
      mapContainerStyle={props.mapContainerStyle}
      center={props.center}
      zoom={12}
      onClick={props.onClick}
      options={{ disableDefaultUI: true }}
    >
      {props.children}
    </GoogleMap>
  )
}

export default React.memo(GoogleMapsComponent)
