import Button from '@mui/material/Button';
import ButtonGroup from '@mui/material/ButtonGroup';
import Box from '@mui/material/Box';
import { useState } from 'react';
import i18n from '../configs/i18n';

export default function LanguageButtons() {
  const languageButtons = ['EN', 'ES']
  const [lang, setLang] = useState<string>(i18n.language)

  const handleLanguage = (lang: string) => {
    setLang(lang);
    i18n.changeLanguage(lang);
    localStorage.setItem('lang', lang)
    window.dispatchEvent(new Event('storage'))
  }

  return (
    <Box
      sx={{
        display: 'flex',
        '& > *': {
          m: 1,
        },
        position: 'absolute',
        right: '10px',
        top: '10px',
        zIndex: '10'
      }}
    >
      <ButtonGroup
        orientation="vertical"
        aria-label="vertical contained button group"
        variant="contained"
      >
        {languageButtons.map((value, key) => (
          <Button
            key={key}
            onClick={() => handleLanguage(value)}
            color={lang === value ? 'primary' : 'inherit'}
          >
            {value}
          </Button>
        ))}
      </ButtonGroup>
    </Box>
  )
}

