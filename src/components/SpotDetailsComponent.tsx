import { Spot } from "../types/Spots";
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import React from "react";
import { SPOTS_TERM } from "../consts/spots";
import Grid from "@mui/material/Grid";
import { useTranslation } from "react-i18next";

interface SpotDetailsProps {
  spot: Spot
}

export default function SpotDetailsComponent({ spot }: SpotDetailsProps) {
  const { t } = useTranslation();
  const url = process.env.REACT_APP_API;
  const characteristics: { [key: string]: string } = {
    location: `${spot.zip_code?.city} - ${spot.zip_code?.municipality}`,
    square_space: spot.square_space ? `${spot.square_space}m²` : '',
    term: spot.term ? t(`${SPOTS_TERM.filter(value => value.value === spot.term.toString())[0]?.name}`) : '',
    description: spot.description ? `${spot.description[0].toUpperCase() + spot.description.slice(1).toLowerCase()}` : ''
  }

  const getSpotImage = (() => {
    //TODO All spots now have the default image because the images in spot.photos apparently are on another server ¿?
    // if (spot.photos) {
    //   if (typeof spot.photos === 'string') {
    //     return spot.photos
    //   }
    //   return spot.photos[0]
    // }
    return 'images/spot-square.jpg'
  })


  const renderCharacteristics = ((key: string, index: number) => {
    if (characteristics[key]) {
      return (
        <React.Fragment key={index}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <Typography variant="h6" color="primary">{t(`${key}`)}</Typography>
              <Typography variant="body1">{characteristics[key]}</Typography>
            </Grid>
          </Grid>
        </React.Fragment>
      )
    }
  })

  return (
    <Grid container spacing={2}>
      <Grid container item md={5} justifyContent="center">
        <CardMedia
          component="img"
          alt={characteristics.name ?? ''}
          height="300px"
          width="auto"
          image={`${url}${getSpotImage()}`}
        />
      </Grid>
      <Grid container item md={7} justifyContent="center" alignItems="flex-start" direction="column">
        <Grid item>
          <Typography variant="h5" color="primary">{spot?.name ?? t('Spot Available')}</Typography>
        </Grid>
        <Grid item>
          <React.Fragment>
            {Object.keys(characteristics).map((key, index) => renderCharacteristics(key, index))}
          </React.Fragment>
        </Grid>
      </Grid>
    </Grid>

  );
}

