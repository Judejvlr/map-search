import React from 'react';
import Grid, { GridProps } from '@mui/material/Grid';
import CircularProgress, { CircularProgressProps } from '@mui/material/CircularProgress';

export default function FancyProgress(props: CircularProgressProps & { ContainerGridProps?: GridProps }) {
  const { ContainerGridProps, ...progressProps } = props;
  return (
    <Grid {...ContainerGridProps} container justifyContent="center" alignItems="center">
      <Grid item sx={{ m: 2 }}>
        <CircularProgress {...progressProps} />
      </Grid>
    </Grid>
  )
};