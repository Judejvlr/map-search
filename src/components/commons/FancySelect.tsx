import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormHelperText from '@mui/material/FormHelperText';
import FormControl from '@mui/material/FormControl';
import Select, { SelectProps } from '@mui/material/Select';
import OutlinedInput from '@mui/material/OutlinedInput';

interface FilterElementSelect {
  selectItems: { value: string, name: string }[];
  helperText?: string,
  testId?: string
};

export default function FancySelect(props: SelectProps & FilterElementSelect) {

  return (
    <FormControl fullWidth={!!props.fullWidth}>
      <InputLabel id={props.labelId}>{props.label}</InputLabel>
      <Select
        labelId={props.labelId}
        id={props.id}
        value={props.value}
        label={props.label}
        input={<OutlinedInput label="props.label" />}
        onChange={props.onChange}
        data-testid={props.testId}
      >
        {(props.selectItems || []).map(item => (
          <MenuItem value={item.value} key={item.value}>{item.name}</MenuItem>
        ))}
      </Select>
      <FormHelperText>{props.helperText}</FormHelperText>
    </FormControl>
  )
}