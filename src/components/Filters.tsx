

import LoadingButton from '@mui/lab/LoadingButton';
import Grid from "@mui/material/Grid";
import SearchIcon from '@mui/icons-material/Search';
import { SPOTS_TERM, SPOTS_TYPES, SQUARE_SPACE } from "../consts/spots";
import FancySelect from "./commons/FancySelect";
import { useTheme } from '@mui/material/styles';
import { useSpots } from "../hooks/useSpots";
import Typography from '@mui/material/Typography';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';


export default function Filters() {
  const { t } = useTranslation();
  const theme = useTheme();
  const { loading, setSpotsParams } = useSpots()
  const [type, setType] = useState<string>('');
  const [term, setTerm] = useState<string>('');
  const [squareSpace, setSquareSpace] = useState<string>('');


  const handleSubmit = (e: any) => {
    e.preventDefault();
    setSpotsParams({ type: type, term: term, square_space: squareSpace })
  }

  return (
    <form className='form' onSubmit={handleSubmit}>
      <Grid container direction="column" spacing={2}
        sx={{
          paddingLeft: theme.spacing(1),
          paddingRight: theme.spacing(1),
          marginTop: theme.spacing(1)
        }
        }
      >
        <Grid item>
          <Typography variant="h4">
            {t('Spot search')}
          </Typography>
        </Grid>
        <Grid item>
          <FancySelect
            fullWidth
            selectItems={SPOTS_TYPES}
            label={t('spot_type')}
            value={type}
            onChange={(e) => setType(e?.target.value as string)}
          />
        </Grid>
        <Grid item>
          <FancySelect
            fullWidth
            selectItems={SPOTS_TERM}
            label={t('term_type')}
            value={term}
            onChange={(e) => setTerm(e?.target.value as string)}
          />
        </Grid>
        <Grid item>
          <FancySelect
            fullWidth
            selectItems={SQUARE_SPACE}
            label={t('size')}
            value={squareSpace}
            onChange={(e) => setSquareSpace(e?.target.value as string)}
          />
        </Grid>
        <Grid container item justifyContent="center" alignContent="center">
          <LoadingButton
            loading={loading}
            loadingPosition="start"
            variant="contained"
            color="primary"
            size="large"
            startIcon={<SearchIcon />}
            type="submit"
          >
            {t('search')}
          </LoadingButton>
        </Grid>
      </Grid >
    </form >
  )
}