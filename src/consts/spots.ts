import i18n from "../configs/i18n"


export const SPOTS_TYPES = [
  { value: '1', name: i18n.t('Street Spot') },
  { value: '2', name: i18n.t('In mall Spot') },
  { value: '3', name: i18n.t('In mall island') },
  { value: '4', name: i18n.t('Other') }
]

export const SPOTS_TERM = [
  { value: '1', name: i18n.t('Short Term') },
  { value: '2', name: i18n.t('Long Term') },
  { value: '3', name: i18n.t('Both') },
]

export const SQUARE_SPACE = [
  { value: '1', name: i18n.t('Less than 50 m²') },
  { value: '2', name: i18n.t('51 - 250 m²') },
  { value: '3', name: i18n.t('More than 250 m²') },
]