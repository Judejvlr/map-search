export interface Zip {
  code: string,
  settlement: string,
  settlement_type: string,
  municipality: string,
  city: string,
  zone: string
}