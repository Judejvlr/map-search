export interface SEO {
  title: string,
  description: string,
  keywords: Array<string>,
  image: string
}