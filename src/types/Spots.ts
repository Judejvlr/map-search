import { SEO } from "./Seo"
import { Zip } from "./Location"

export interface Spot {
  id: number,
  alias: string | null,
  city_id: number,
  description: string | null,
  endorsement: null,
  ext_number: string,
  floor_level: null,
  front: number,
  grace_time: null,
  guarantee_deposit: null,
  has_validated_prespot: number,
  height: number,
  in_corner: null,
  int_number: null,
  latitude: number,
  level: number,
  listing_id: null,
  longitude: number,
  name: string,
  payload: null,
  pdf_description: null,
  people_flow: null,
  people_flow_term: number,
  phone: null,
  photos: null | string | Array<string>,
  reference: string,
  relative_url: string,
  rent_in_advance: null,
  seo: SEO,
  square_space: string,
  street: string,
  term: number,
  total_favorites: number,
  uuid: string,
  visual_blocks: null,
  zip_code: Zip,
  created_at: string,
  updated_at: string
}

export interface SpotParams {
  type?: string,
  term?: string,
  square_space?: string,
}