import { useState } from 'react';
import './App.css';
import { SpotsContext } from './context/SpotsContext';
import { Spot } from './types/Spots';
import MapView from './views/MapView';
import './configs/i18n';
import LanguageButtons from './components/LanguageButtons';


function App() {
  const [spots, setSpots] = useState<Array<Spot>>([])

  return (
    <SpotsContext.Provider value={{ spots, setSpots }}>
      <LanguageButtons />
      <MapView />
    </SpotsContext.Provider>
  );
}

export default App;


