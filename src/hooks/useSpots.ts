import { useCallback, useContext, useEffect, useState } from 'react'
import { SpotsContext } from '../context/SpotsContext';
import { fetchSpots } from '../services/getSpots';
import { SpotParams } from '../types/Spots';

export function useSpots() {
  const [loading, setLoading] = useState(false)
  const { spots, setSpots } = useContext(SpotsContext)
  const [spotsParams, setSpotsParams] = useState<SpotParams>({});

  const handleSpotsParams = useCallback(() => {
    let params: URLSearchParams = new URLSearchParams()
    if (spotsParams.type) params.append('type', spotsParams.type);
    if (spotsParams.term) params.append('term', spotsParams.term);
    if (spotsParams.square_space) params.append('square_space', spotsParams.square_space);
    return params
  }, [spotsParams])


  useEffect(function () {
    let params = handleSpotsParams();
    setLoading(true)
    fetchSpots(params)
      .then((response) => {
        setSpots(response)
        setLoading(false)
      })
  }, [setSpots, handleSpotsParams])

  return { loading, spots, setSpotsParams }
}