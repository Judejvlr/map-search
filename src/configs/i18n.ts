import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import { EN } from '../consts/languages/en';
import { ES } from '../consts/languages/es';

i18n
  .use(initReactI18next)
  .init({
    resources: {
      ES: {
        translation: {
          ...ES
        }
      },
      EN: {
        translation: {
          ...EN
        }
      }
    },
    lng: localStorage.getItem('lang') ?? 'ES',
    fallbackLng: "ES",

    interpolation: {
      escapeValue: false
    }
  });

export default i18n;